package http

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/types"
	"accelera-custom-actions/utils"
	"bytes"
	"crypto/tls"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
	_ "strconv"
)

// OffersService - сервис для отправки REST для offer
func OffersService(id string, ctx map[string]interface{}, result *int, resultName *string) {
	log.Info("OfferService")

	var profileIdCtx string
	temp, ok := ctx["profile_id"]
	if ok {
		profileIdCtx = temp.(string)
	}

	//	test for empty profile_id
	var profileId string
	if len(profileIdCtx) == 0 && len(id) == 0 {
		logger.Log.Error("AwardType is empty")
		*result = -1
		return
	} else {
		if len(profileIdCtx) != 0 {
			profileId = profileIdCtx
		} else {
			profileId = id
		}
	}
	logger.Log.Info("Profile id: ", profileId)

	//	http request
	var restReq types.OfferToRestReq
	var respFromRest types.OfferFromRestResp

	//restReq.ProfileId = "3fa85f64-5717-4562-b3fc-2c963f66afa6"
	//restReq.Selector.Names = []string{"Personal_Offers"}
	restReq.Selector.Names = []string{"Accelera_Personal_Offer"}
	restReq.User.Id = profileId
	restReq.Session.Custom = "myCustomSession345"
	restReq.Context.Page.Type = "HOMEPAGE"
	restReq.Context.Page.Location = "https://example.org"
	restReq.Context.Page.Data = []string{}
	restReq.Options.ReturnAnalyticsMetadata = false
	restReq.Options.IsImplicitPageview = false

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	//	объявляем клиента для запроса
	client := &http.Client{Transport: tr}

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	//	дергаем сервис
	req, err := http.NewRequest("POST", utils.Cfg.OfferUrl, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")
	//req.Header.Set("dy-api-key", utils.Cfg.DY_API_KEY)
	req.Header.Set("Authorization", "Bearer "+utils.Cfg.OFFER_BEARER_KEY)

	if err != nil {
		log.Error("Error with New Request", err)
	} else {
		logger.Log.Warn("Creating request is fine", bytes.NewBuffer(body))
	}

	//	отправляем запрос
	resp, err := client.Do(req)
	if err != nil {
		log.Error("error throw sending")
	} else {
		if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
			log.Error("Aware Answer from Rst with error: ", err)
			//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
		} else {
			logger.Log.Warn("respFromRest: ", respFromRest.Choices)
		}
	}

	if len(respFromRest.Choices) > 0 {

		*result = respFromRest.Choices[0].Variations[0].Payload.Data.Slots[0].ProductData.Sku
		*resultName = respFromRest.Choices[0].Variations[0].Payload.Data.Slots[0].ProductData.Name
	}

}
