package http

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/types"
	"accelera-custom-actions/utils"
	"bytes"
	"crypto/tls"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	_ "strconv"
)

// AwardService - сервис для отправки REST для award
func AwardService(id string, _data interface{}, ctx map[string]interface{}, result *int) {
	logger.Log.Info("AwardService")

	var data = _data.(map[string]interface{})
	var awardTypeData = data["award_type"].(string)

	var awardTypeCtx string
	var profileIdCtx string
	temp, ok := ctx["award_type"]
	if ok {
		awardTypeCtx = temp.(string)
	}
	temp, ok = ctx["profile_id"]
	if ok {
		profileIdCtx = temp.(string)
	}

	//	test for empty award_type
	var awardType string
	if len(awardTypeData) == 0 && len(awardTypeCtx) == 0 {
		logger.Log.Error("AwardType is empty")
		*result = -1
		return
	} else {
		if len(awardTypeData) != 0 {
			awardType = awardTypeData
		} else {
			awardType = awardTypeCtx
		}
	}

	//	test for empty profile_id
	var profileId string
	if len(profileIdCtx) == 0 && len(id) == 0 {
		logger.Log.Error("AwardType is empty")
		*result = -1
		return
	} else {
		if len(profileIdCtx) != 0 {
			profileId = profileIdCtx
		} else {
			profileId = id
		}
	}

	logger.Log.Warn("awareType", awardType)
	logger.Log.Warn("profileId", profileId)

	awareId, _ := strconv.ParseInt(awardType, 10, 0)

	log.Error("awareId", awareId)

	//	http request
	var restReq types.AwardToRestReq
	var respFromRest types.AwardFromRestResp

	//restReq.ProfileId = "3fa85f64-5717-4562-b3fc-2c963f66afa6"
	// if
	restReq.ProfileId = profileId
	restReq.MechanicId = int(awareId)
	restReq.AsAward = true
	restReq.AdditionalInfo.Partner = "Accelera" //                     //

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	//	объявляем клиента для запроса
	client := &http.Client{Transport: tr}

	body, err := json.Marshal(restReq)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	//	дергаем сервис
	req, err := http.NewRequest("POST", utils.Cfg.AwardUrl, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-Key", utils.Cfg.X_API_KEY)

	if err != nil {
		log.Error("Error with New REquest", err)
	} else {
		logger.Log.Warn("Creating request is fine", bytes.NewBuffer(body))
	}

	//	отправляем запрос
	resp, err := client.Do(req)
	if err != nil {
		log.Error("error throw sending")
	} else {
		log.Info("Response RAW: ", resp.Body)
		if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
			log.Error("Aware Answer from Rst with error: ", err)
			//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
		} else {
			logger.Log.Info("Response from REST", respFromRest)
		}
	}

	*result = respFromRest.OfferId
}
