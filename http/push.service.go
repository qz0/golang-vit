package http

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/types"
	"accelera-custom-actions/utils"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/aymerick/raymond"
	"net/http"
)

// PushService - сервис для отправки REST для push_award
// func PushService(id string, eventName string, blockLabel int, ctx map[string]interface{}, context types.ContextFromRabbit, _data interface{}, flowId string, result *[]byte) {
func PushService(id string, eventName string, blockLabel int, ctx map[string]interface{}, _data interface{}, flowId string, result *[]byte) {
	logger.Log.Warn("PushAwardService")

	//	http request
	var err error
	var body []byte

	var pushTitleCtx string
	var pushTextCtx string
	var pushLabelCtx string
	var pushProfileCtx string
	var pushDeviceCtx string
	var pushOfferIdCtx string

	temp, ok := ctx["title"]
	if ok {
		pushTitleCtx = temp.(string)
	}
	temp, ok = ctx["text"]
	if ok {
		pushTextCtx = temp.(string)
	}
	temp, ok = ctx["label"]
	if ok {
		pushLabelCtx = temp.(string)
	}
	temp, ok = ctx["profile_id"]
	if ok {
		pushProfileCtx = temp.(string)
	}
	temp, ok = ctx["device_id"]
	if ok {
		pushDeviceCtx = temp.(string)
		if len(pushDeviceCtx) > 0 {
			pushProfileCtx = ""
		}
	}
	temp, ok = ctx["offer_id"]
	if ok {
		pushOfferIdCtx = temp.(string)
	}

	logger.Log.Info("pushId: ", id)
	logger.Log.Info("eventName: ", eventName)
	logger.Log.Info("pushTitleCtx: ", pushTitleCtx)
	logger.Log.Info("pushTextCtx: ", pushTextCtx)
	logger.Log.Info("pushLabelCtx: ", pushLabelCtx)
	logger.Log.Info("pushProfileCtx: ", pushProfileCtx)
	logger.Log.Info("pushDeviceCtx: ", pushDeviceCtx)
	logger.Log.Info("pushOfferIdCtx: ", pushOfferIdCtx)

	var data = _data.(map[string]interface{})
	var pushTitleData = data["title"].(string)
	var pushTextData = data["text"].(string)
	var pushLabelData = data["label"].(string)

	logger.Log.Info("data.Title: ", pushTitleData)
	logger.Log.Info("data.Text: ", pushTextData)
	logger.Log.Info("data.Label: ", pushLabelData)

	var pushTitle string
	if len(pushTitleCtx) == 0 {
		pushTitle = pushTitleData
	} else {
		pushTitle = pushTitleCtx
	}

	var pushText string
	if len(pushTextCtx) == 0 {
		pushText = pushTextData
	} else {
		pushText = pushTextCtx
	}

	pushTitle, err = raymond.Render(pushTitle, ctx)
	if err != nil {
		logger.Log.Warn("Error in title render")
	} else {
		logger.Log.Info("renderedText: ", pushTitle)
	}

	pushText, err = raymond.Render(pushText, ctx)
	if err != nil {
		logger.Log.Warn("Error in text render")
	} else {
		logger.Log.Info("renderedTitle: ", pushText)
	}

	var pushLabel string
	if len(pushLabelData) == 0 {
		pushLabel = pushLabelData
	} else {
		pushLabel = pushLabelData
	}

	logger.Log.Info("pushTitle: ", pushTitle)
	logger.Log.Info("pushText: ", pushText)
	logger.Log.Info("pushLabel: ", pushLabel)

	var profileId string
	var deviceId string

	//	is Device_id
	if eventName == utils.Cfg.PushDeviceEventName {
		logger.Log.Info("first if device_id ")
		if len(pushDeviceCtx) == 0 {
			deviceId = id
		} else {
			deviceId = pushDeviceCtx
		}
	} else {
		logger.Log.Info("first if profile_id ")
		if len(pushProfileCtx) == 0 {
			profileId = id
		} else {
			profileId = pushProfileCtx
		}
	}
	logger.Log.Info("profileId: ", profileId)
	logger.Log.Info("deviceId: ", profileId)

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	//	объявляем клиента для запроса
	client := &http.Client{Transport: tr}

	var dataJson string

	switch eventName {
	case utils.Cfg.PushOffersEventName: // "пуш с оффером и переходом в раздел"
		//restReq.Data.Json = `{"transition":{"direction":"app_screen","screen": {"type":"offer_card","value":{"id":"` + context.OfferId + `"}}}}`
		dataJson = `{"transition":{"direction":"app_screen","screen": {"type":"app_section","value":{"id":"offers"}}}}`
		break

	case utils.Cfg.PushAwardEventName: //"Пуш при нажатии ведет\nна один из разделов МП":
		dataJson = `{"transition":{"direction":"app_screen","screen":{"type":"offer_card","value":{"id":"` + pushOfferIdCtx + `"}}}}`
		break

	case utils.Cfg.PushMessageEventName, utils.Cfg.PushDeviceEventName: //"Пуш без перехода на один из разделов МП"
		dataJson = `{"transition":{"direction":"without_transition"}}`
		break
	}
	logger.Log.Info("Data.Json: ", dataJson)
	logger.Log.Info("eventName: ", eventName)

	if eventName == utils.Cfg.PushDeviceEventName {
		logger.Log.Info("second if device_id ")
		var restReq types.PushAwardToRestReqDevice
		//	make struct
		restReq.DeviceId = deviceId
		restReq.Notification.Title = pushTitle
		restReq.Notification.Body = pushText
		restReq.Data.Type = "screen"
		restReq.FcmOptions.AnalyticsLabel = createLabel(flowId, pushLabel)
		restReq.Data.Json = dataJson

		logger.Log.Info("restReq: ")

		body, err = json.Marshal(restReq)
		logger.Log.Info("body: ", string(body))
		if err != nil {
			logger.Log.Error("Error with Marshaling", err)
		} else {
			logger.Log.Warn("Marshaling is fine: ", restReq)
		}
	} else {
		logger.Log.Info("second if profile_id ")
		var restReq types.PushAwardToRestReqProfile
		//	make struct
		restReq.ProfileId = profileId
		restReq.Notification.Title = pushTitle
		restReq.Notification.Body = pushText
		restReq.Data.Type = "screen"
		restReq.FcmOptions.AnalyticsLabel = createLabel(flowId, pushLabel)
		restReq.Data.Json = dataJson

		body, err = json.Marshal(restReq)
		logger.Log.Info("body: ", string(body))
		if err != nil {
			logger.Log.Error("Error with Marshaling", err)
		} else {
			logger.Log.Warn("Marshaling is fine: ", restReq)
		}
	}

	//	дергаем сервис
	req, err := http.NewRequest("POST", utils.Cfg.PushUrl, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-Key", utils.Cfg.X_API_KEY)

	if err != nil {
		logger.Log.Error("Error with New Request", err)
	} else {
		logger.Log.Warn("Creating request is fine", bytes.NewBuffer(body))
	}

	//	отправляем запрос
	resp, err := client.Do(req)
	if err != nil {
		logger.Log.Error("error throw sending")
	} else {

		logger.Log.Info("EVENT: ", eventName)
		switch eventName {
		case utils.Cfg.PushOffersEventName: // "пуш с оффером и переходом в раздел"
			var respFromRest types.PushRedirectOfferFromRestResp

			if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
				logger.Log.Error("Aware Answer from Rst with error: ", err)
				//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
			} else {
				logger.Log.Warn("Response from REST: ", respFromRest)
			}
			break

		case utils.Cfg.PushAwardEventName: //"Пуш при нажатии ведет\nна один из разделов МП":
			var respFromRest types.PushRedirectFromRestResp

			if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
				logger.Log.Error("Award Answer from Rst with error: ", err)
				//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
			} else {
				logger.Log.Warn("Response from REST: ", respFromRest)
			}
			break

		case utils.Cfg.PushMessageEventName, utils.Cfg.PushDeviceEventName: //"Пуш без перехода на один из разделов МП"
			var respFromRest types.PushWithoutRedirectFromRestResp

			if err := json.NewDecoder(resp.Body).Decode(&respFromRest); err != nil {
				logger.Log.Error("Aware Answer from Rst with error: ", err)
				//http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
			} else {
				logger.Log.Warn("Response from REST: ", respFromRest)

			}
			break

		}

		logger.Log.Info("Status: ", resp.Status)

	}

}

// createLabel - create analitic label for rest
func createLabel(flowId string, target string) string {

	var label = fmt.Sprintf("%s_%s", target, flowId)
	logger.Log.Warn("label", label)
	return label
}
