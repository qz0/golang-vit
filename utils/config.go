package utils

import (
	"accelera-custom-actions/logger"
	_ "github.com/caarlos0/env/v6"
	"github.com/spf13/viper"
)

type Config struct {
	LogLevel             string `mapstructure:"LOG_LEVEL"`
	X_API_KEY            string `mapstructure:"X_API_KEY"`
	OFFER_BEARER_KEY     string `mapstructure:"OFFER_BEARER_KEY"`
	RedisConnection      string `mapstructure:"REDIS_CONNECTION"`
	RedisHost            string `mapstructure:"REDIS_HOST"`
	RedisPort            string `mapstructure:"REDIS_PORT"`
	BrokerConnection     string `mapstructure:"BROKER_CONNECTION"`
	RabbitUsername       string `mapstructure:"RABBIT_USERNAME"`
	RabbitPassword       string `mapstructure:"RABBIT_PASSWORD"`
	RabbitHost           string `mapstructure:"RABBIT_HOST"`
	RabbitPort           string `mapstructure:"RABBIT_PORT"`
	ExchangeName         string `mapstructure:"EXCHANGE_NAME"`
	AwardQueueName       string `mapstructure:"AWARD_QUEUE_NAME"`
	AwardEventName       string `mapstructure:"AWARD_EVENT_NAME"`
	PushMessageQueueName string `mapstructure:"PUSH_MESSAGE_QUEUE_NAME"`
	PushMessageEventName string `mapstructure:"PUSH_MESSAGE_EVENT_NAME"`
	PushDeviceQueueName  string `mapstructure:"PUSH_DEVICE_QUEUE_NAME"`
	PushDeviceEventName  string `mapstructure:"PUSH_DEVICE_EVENT_NAME"`
	PushOffersQueueName  string `mapstructure:"PUSH_OFFERS_QUEUE_NAME"`
	PushOffersEventName  string `mapstructure:"PUSH_OFFERS_EVENT_NAME"`
	PushAwardQueueName   string `mapstructure:"PUSH_AWARD_QUEUE_NAME"`
	PushAwardEventName   string `mapstructure:"PUSH_AWARD_EVENT_NAME"`
	OfferQueueName       string `mapstructure:"OFFER_QUEUE_NAME"`
	OfferEventName       string `mapstructure:"OFFER_EVENT_NAME"`
	AwardUrl             string `mapstructure:"AWARD_URL"`
	OfferUrl             string `mapstructure:"OFFER_URL"`
	PushUrl              string `mapstructure:"PUSH_URL"`
	Version              string `mapstructure:"VERSION"`
}

type EnvConfig struct {
	X_API_KEY        string `env:"X_API_KEY"`
	OFFER_BEARAR_KEY string `env:"OFFER_BEARAR_KEY"`
	RedisConnection  string `env:"REDIS_CONNECTION"`
	BrokerConnection string `env:"BROKER_CONNECTION"`
}

var Cfg Config
var envCfg EnvConfig

//var RMQ types.RabbitConf

// LoadConfig reads configuration from file or environment variables.
func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	logger.Log.Error(config)

	return
}
