package redis

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/utils"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"strings"
)

// structure for actions
type Action struct {
	name   string
	config string
}

var ctx = context.Background()

// ActionRegistration - registrtion all actions in redis
func ActionRegistration() {

	// list of actions for registration
	var actions = []Action{
		{ //	AWARD
			name: utils.Cfg.AwardEventName,
			config: `{
		  "title":"Award request",
		  "name":"` + utils.Cfg.AwardEventName + `",
		  "class":"stateless-action",
		  "inputs":1,
		  "outputs":0,
		  "data":{
			"isCustom":true,
			"returnable":false,
			"award_type":""
		  },
		  "paramsDescription":[
			{
			  "name":"award_type",
			  "value":"Укажите необходимый SKU/ код mechanic_id"
			}
		  ]
		}`,
		},
		//	OFFER
		{
			name: utils.Cfg.OfferEventName,
			config: `{
			  "title":"Offer request",
			  "name":"` + utils.Cfg.OfferEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false
			  },
			  "paramsDescription":[
			  ]
			}`,
		},
		//	PUSH OFFERS
		{
			name: utils.Cfg.PushOffersEventName,
			config: `{
			  "title":"Push offers",
			  "name":"` + utils.Cfg.PushOffersEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false,
				"title":"",
				"text":"",
				"label":""
			  },
			  "paramsDescription":[
				{
				  "name":"title",
				  "value":"Заголовок Push-уведомления"
				},
				{
				  "name":"text",
				  "value":"Тело запроса Push-уведомления"
				},
				{
				  "name":"label",
				  "value":"Метка для аналитики"
				}
			  ]
			}`,
		},
		//	PUSH AWARD
		{
			name: utils.Cfg.PushAwardEventName,
			config: `{
			  "title":"Push award",
			  "name":"` + utils.Cfg.PushAwardEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false,
				"title":"",
				"text":"",
				"label":""
			  },
			  "paramsDescription":[
				{
				  "name":"title",
				  "value":"Заголовок Push-уведомления"
				},
				{
				  "name":"text",
				  "value":"Тело запроса Push-уведомления"
				},
				{
				  "name":"label",
				  "value":"Метка для аналитики"
				}
			  ]
			}`,
		},
		//	PUSH MESSAGE
		{
			name: utils.Cfg.PushMessageEventName,
			config: `{
			  "title":"Push message",
			  "name":"` + utils.Cfg.PushMessageEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false,
				"title":"",
				"text":"",
				"label":""
			  },
			  "paramsDescription":[
				{
				  "name":"title",
				  "value":"Заголовок Push-уведомления"
				},
				{
				  "name":"text",
				  "value":"Тело запроса Push-уведомления"
				},
				{
				  "name":"label",
				  "value":"Метка для аналитики"
				}
			  ]
			}`,
		},
		//	PUSH DEVICE
		{
			name: utils.Cfg.PushDeviceEventName,
			config: `{
			  "title":"Push device",
			  "name":"` + utils.Cfg.PushDeviceEventName + `",
			  "class":"stateless-action",
			  "inputs":1,
			  "outputs":0,
			  "data":{
				"isCustom":true,
				"returnable":false,
				"title":"",
				"text":"",
				"label":""
			  },
			  "paramsDescription":[
				{
				  "name":"title",
				  "value":"Заголовок Push-уведомления"
				},
				{
				  "name":"text",
				  "value":"Тело запроса Push-уведомления"
				},
				{
				  "name":"label",
				  "value":"Метка для аналитики"
				}
			  ]
			}`,
		},
	}

	var actions_old = []string{
		"award_event",
		"offer_event",
		"push_message_event",
		"push_device_event",
		"push_offers_event",
		"push_award_event",
	}

	logger.Log.Warn("Redis - Connection string: ", utils.Cfg.RedisConnection)
	var index = strings.Index(utils.Cfg.RedisConnection, "redis://")
	if index == 0 {
		utils.Cfg.RedisConnection = utils.Cfg.RedisConnection[9:]
	}

	index = strings.Index(utils.Cfg.RedisConnection, "@")
	var password string
	var address string
	if index >= 0 {
		password = utils.Cfg.RedisConnection[:index]
		address = utils.Cfg.RedisConnection[index+1:]
	} else {
		address = utils.Cfg.RedisConnection
	}
	logger.Log.Warn("Redis - address: ", address)
	logger.Log.Warn("Redis - password: ", password)

	//	connect to redis
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       0,
	})

	//
	for _, action := range actions_old {
		_, err := client.HDel(ctx, "platform:actions", action).Result()
		// handle the error
		if err != nil {
			fmt.Println(err)
		}
	}

	//	all actions in range
	for _, action := range actions {
		_, err := client.HSet(ctx, "platform:actions", action.name, action.config).Result()
		// handle the error
		if err != nil {
			fmt.Println(err)
		}
	}
}
