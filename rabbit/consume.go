package rabbit

import (
	"accelera-custom-actions/http"
	"accelera-custom-actions/logger"
	"accelera-custom-actions/types"
	"accelera-custom-actions/utils"
	"encoding/json"
	log "github.com/sirupsen/logrus"
)

// GetMessage - получение сообщения
func GetMessage(qName string) {
	logger.Log.Info(qName)

	var rc RabbitConnection
	rc.Consume(qName, messageHandle)

	//logger.Log.Warn(message)

}

func messageHandle(message interface{}) error {
	logger.Log.Warn("Version: ", utils.Cfg.Version)
	logger.Log.Warn("Config: ", utils.Cfg)

	var data types.RequestFromRabbit
	var result int
	var resultName string
	var body []byte

	logger.Log.Warn("before marshal", string(message.([]byte)))
	//logger.Log.Warn("after marshal", temp)
	err := json.Unmarshal(message.([]byte), &data)
	if err != nil {
		log.Error("Error: ", err)
	}
	//
	log.Info("after marshal", data)
	log.Warn("data.Action.Data", data.Action.Data)

	//
	switch data.Action.Name {
	case utils.Cfg.AwardEventName:
		logger.Log.Info("switch -> award")
		http.AwardService(data.Id, data.Action.Data, data.Context, &result)
		// контекст /Ид - профиль ид
		//	orderId := <-ch
		if result == -1 {
			return nil
		}
		produceAward(result, data.Id)
		break
	case utils.Cfg.OfferEventName:
		logger.Log.Info("switch -> offers")
		http.OffersService(data.Id, data.Context, &result, &resultName)
		//	orderId := <-ch
		if result == -1 || resultName == "" {
			return nil
		}
		//	orderId := <-ch
		produceOffer(result, resultName, data.Id)
		break
	case utils.Cfg.PushMessageEventName, utils.Cfg.PushDeviceEventName, utils.Cfg.PushOffersEventName, utils.Cfg.PushAwardEventName:
		logger.Log.Info("switch -> " + data.Action.Name)
		http.PushService(data.Id, data.Action.Name, data.Action.Id, data.Context, data.Action.Data, data.FlowId, &body)
		break
	}

	return nil
}
