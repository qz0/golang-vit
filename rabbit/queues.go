package rabbit

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/utils"
	"fmt"
	"github.com/streadway/amqp"
)

// QueuesRegistration - binding queues in exchanges
func QueuesRegistration() {
	// queues
	var queues = []string{
		utils.Cfg.AwardQueueName,
		utils.Cfg.OfferQueueName,
		utils.Cfg.PushDeviceQueueName,
		utils.Cfg.PushMessageQueueName,
		utils.Cfg.PushOffersQueueName,
		utils.Cfg.PushAwardQueueName,
	}

	// Create a new RabbitMQ connection.
	//connectRabbitMQ, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", utils.Cfg.RabbitUsername, utils.Cfg.RabbitPassword, utils.Cfg.RabbitHost, utils.Cfg.RabbitPort))
	connectRabbitMQ, err := amqp.Dial(utils.Cfg.BrokerConnection)
	if err != nil {
		panic(err)
	}
	defer connectRabbitMQ.Close()

	// Let's start by opening a channel to our RabbitMQ
	// instance over the connection we have already
	// established.
	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}
	defer channelRabbitMQ.Close()

	for _, queue := range queues {
		logger.Log.Warn("declaring =>", queue)
		// Create a binding between queue and exchanges to rabbitmq
		q, err := channelRabbitMQ.QueueDeclare(queue, true, false, false, false, amqp.Table{"x-queue-mode": "lazy"})
		logger.Log.Warn("declared =>", q.Name)
		//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
		if err != nil {
			fmt.Println(err)
		}
		// Create a binding between queue and exchanges to rabbitmq
		err = channelRabbitMQ.QueueBind(queue, "", "ex-custom-actions", false, nil)
		//err := channelRabbitMQ.QueueBind(queue, "", "award", false, nil)
		if err != nil {
			fmt.Println(err)
		} else {
			logger.Log.Warn("has bind =>", q.Name)
		}
	}
}
