package rabbit

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/types"
	"accelera-custom-actions/utils"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
)

// produceAward -	публикация результатов сценария award
func produceAward(offerId int, eventId string) {
	logger.Log.Info("produceAward")
	var rabbitResp types.AwardRabbitResp
	rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
	rabbitResp.Event = utils.Cfg.AwardEventName
	rabbitResp.Id = eventId
	message, err := json.Marshal(rabbitResp)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	if SendMessage(utils.Cfg.ExchangeName, eventId, string(message[:])) != nil {
		log.Error("Rabbit sending Error")
	} else {
		logger.Log.Warn("Rabbit sending Success: ", string(message[:]))
	}

	logger.Log.Warn("whats all")
}

// produceOffer -	публикация результатов сценария offer
func produceOffer(offerId int, offerName string, eventId string) {
	logger.Log.Info("produceOffer")
	var rabbitResp types.OfferRabbitResp
	rabbitResp.Context.OfferId = fmt.Sprintf("%d", offerId)
	rabbitResp.Context.OfferName = offerName
	rabbitResp.Event = utils.Cfg.OfferEventName
	rabbitResp.Id = eventId
	message, err := json.Marshal(rabbitResp)
	if err != nil {
		log.Error("Error with Marshaling", err)
	} else {
		logger.Log.Warn("Marshaling is fine")
	}

	if SendMessage(utils.Cfg.ExchangeName, eventId, string(message[:])) != nil {
		log.Error("Rabbit sending Error")
	} else {
		logger.Log.Warn("Rabbit sending Success, message: ", string(message[:]))
	}

	logger.Log.Warn("whats all")
}
