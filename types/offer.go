package types

// Request to REST
type OfferToRestReq struct {
	Selector struct {
		Names []string `json:"names"`
	} `json:"selector"`
	User struct {
		Id string `json:"id"`
	} `json:"user"`
	Session struct {
		Custom string `json:"custom"`
	} `json:"session"`
	Context struct {
		Page struct {
			Type     string   `json:"type"`
			Location string   `json:"location"`
			Data     []string `json:"data"`
		} `json:"page"`
	} `json:"context"`
	Options struct {
		IsImplicitPageview      bool `json:"isImplicitPageview"`
		ReturnAnalyticsMetadata bool `json:"returnAnalyticsMetadata"`
	} `json:"options"`
}

// From Rest response
type OfferFromRestResp struct {
	Choices []struct {
		Id         int `json:"id"`
		Variations []struct {
			Id      int `json:"id"`
			Payload struct {
				Data struct {
					Slots []struct {
						Sku         int `json:"sku"`
						ProductData struct {
							Sku  int    `json:"sku"`
							Name string `json:"name"`
						} `json:"productData"`
					} `json:"slots"`
				} `json:"data"`
			} `json:"payload"`
		} `json:"variations"`
	} `json:"choices"`
}

// award
// Request from Rabbit
type OfferFromRabbitReq struct {
	ProfileId string `json:"profile_id"`
}

type OfferRabbitResp struct {
	Id      string               `json:"id"`
	Event   string               `json:"event"`
	Context OfferContextToRabbit `json:"context"`
}

type OfferData struct {
	IsCustom   bool `json:"isCustom"`
	Returnable bool `json:"returnable"`
}

type OfferContextFromRabbit struct {
	ProfileId string `json:"profile_id"`
}
type OfferContextToRabbit struct {
	OfferId   string `json:"offer_id"`
	OfferName string `json:"offer_name"`
}
