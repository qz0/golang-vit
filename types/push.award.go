package types

//	Types for push_award scenario
//
// Request to REST
type PushAwardToRestReq struct {
	DeviceId     string `json:"device_id"`
	ProfileId    string `json:"profile_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// for device_id
type PushAwardToRestReqDevice struct {
	DeviceId     string `json:"device_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// for profile_id
type PushAwardToRestReqProfile struct {
	ProfileId    string `json:"profile_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// From Rest response
type PushAwardFromRestResp struct {
	Transition struct {
		Direction string `json:"direction"`
		Screen    struct {
			Type  string `json:"type"`
			Value struct {
				Id string `json:"id"`
			} `json:"value"`
		} `json:"screen"`
	} `json:"transition"`
}

// award
// Request from Rabbit
type PushAwardFromRabbitReq struct {
	Title bool `json:"title"`
	Text  bool `json:"text"`
	Label bool `json:"label"`
}

// response to Rabbit
type PushAwardRabbitResp struct {
	Id      string `json:"id"`
	Event   string `json:"event"`
	Context struct {
		OfferId string `json:"offer_id"`
	} `json:"context"`
}

// Rabbit Request Data
type PushAwardData struct {
	IsCustom   bool `json:"isCustom"`
	Returnable bool `json:"returnable"`
	Title      bool `json:"title"`
	Text       bool `json:"text"`
	Label      bool `json:"label"`
}

// Rabbit Request Context
type PushAwardContextFromRabbit struct {
	Title     string `json:"title"`
	Text      string `json:"text"`
	Label     string `json:"label"`
	ProfileId string `json:"profile_id"`
}
