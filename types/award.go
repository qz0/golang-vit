package types

// Request to REST
type AwardToRestReq struct {
	ProfileId      string             `json:"profile_id"`
	MechanicId     int                `json:"mechanic_id"`
	AsAward        bool               `json:"as_award"`
	AdditionalInfo AdditionalInfoType `json:"additional_info"`
}

type AdditionalInfoType struct {
	Partner string `json:"partner"`
}

// From Rest response
type AwardFromRestResp struct {
	OfferId int `json:"offerId"`
}

// award
// Request from Rabbit
type AwardFromRabbitReq struct {
	IsCustom   bool   `json:"isCustom"`
	Returnable bool   `json:"returnable"`
	AwardType  string `json:"award_type"`
}

type AwardRabbitResp struct {
	Id      string               `json:"id"`
	Event   string               `json:"event"`
	Context AwardContextToRabbit `json:"context"`
}

type AwardData struct {
	IsCustom   bool   `json:"isCustom"`
	Returnable bool   `json:"returnable"`
	AwardType  string `json:"award_type"`
}

type AwardContextToRabbit struct {
	OfferId string `json:"offer_id"`
}

type AwardContextFromRabbit struct {
	AwardType string `json:"award_type"`
	ProfileId string `json:"profile_id"`
}
