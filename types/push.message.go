package types

//	Types for push_message scenario
//
// Request to REST
type PushMessageToRestReq struct {
	ProfileId    string `json:"profile_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// From Rest response
type PushMessageFromRestResp struct {
	Transition struct {
		Direction string `json:"direction"`
	} `json:"transition"`
}

// award
// Request from Rabbit
type PushMessageFromRabbitReq struct {
	Title bool `json:"title"`
	Text  bool `json:"text"`
	Label bool `json:"label"`
}

// response to Rabbit
type PushMessageRabbitResp struct {
	Id      string `json:"id"`
	Event   string `json:"event"`
	Context struct {
		OfferId string `json:"offer_id"`
	} `json:"context"`
}

// Rabbit Request Data
type PushMessageData struct {
	IsCustom   bool `json:"isCustom"`
	Returnable bool `json:"returnable"`
	Title      bool `json:"title"`
	Text       bool `json:"text"`
	Label      bool `json:"label"`
}

// Rabbit Request Context
type PushMessageContextFromRabbit struct {
	Title     string `json:"title"`
	Text      string `json:"text"`
	Label     string `json:"label"`
	ProfileId string `json:"profile_id"`
}
