package types

//	Types for push_offer scenario
//
// Request to REST
type PushOfferToRestReq struct {
	ProfileId      string `json:"profile_id"`
	MechanicId     int    `json:"mechanic_id"`
	Title          bool   `json:"title"`
	Text           bool   `json:"text"`
	Label          bool   `json:"label"`
	AdditionalInfo struct {
		Partner string `json:"partner"`
	} `json:"additional_info"`
}

// From Rest response
type PushOfferFromRestResp struct {
	ProfileId    string `json:"profile_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// Request from Rabbit
type PushOfferFromRabbitReq struct {
	Title bool `json:"title"`
	Text  bool `json:"text"`
	Label bool `json:"label"`
}

// response to Rabbit
type PushOfferRabbitResp struct {
	Id      string `json:"id"`
	Event   string `json:"event"`
	Context struct {
		OfferId string `json:"offer_id"`
	} `json:"context"`
}

// Rabbit Request Data
type PushOfferData struct {
	IsCustom   bool `json:"isCustom"`
	Returnable bool `json:"returnable"`
	Title      bool `json:"title"`
	Text       bool `json:"text"`
	Label      bool `json:"label"`
}

// Rabbit Request Context
type PushOfferContextFromRabbit struct {
	Title     string `json:"title"`
	Text      string `json:"text"`
	Label     string `json:"label"`
	ProfileId string `json:"profile_id"`
}
