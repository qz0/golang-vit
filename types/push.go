package types

//	Types for push_award scenario
//
// Request to REST
type PushToRestReq struct {
	ProfileId    string `json:"profile_id"`
	Notification struct {
		Title string `json:"title"`
		Body  string `json:"body"`
	} `json:"notification"`
	FcmOptions struct {
		AnalyticsLabel string `json:"analytics_label"`
	} `json:"fcm_options"`
	Data struct {
		Type string `json:"type"`
		Json string `json:"json"`
	} `json:"data"`
}

// From Rest response
type PushWithoutRedirectFromRestResp struct {
	Transition struct {
		Direction string `json:"direction"`
		Screen    struct {
			Type  string `json:"type"`
			Value struct {
				Id string `json:"id"`
			} `json:"value"`
		} `json:"screen"`
	} `json:"transition"`
}

type PushRedirectFromRestResp struct {
	Transition struct {
		Direction string `json:"direction"`
		Screen    struct {
			Type  string `json:"type"`
			Value struct {
				Id string `json:"id"`
			} `json:"value"`
		} `json:"screen"`
	} `json:"transition"`
}

type PushRedirectOfferFromRestResp struct {
	Transition struct {
		Direction string `json:"direction"`
		Screen    struct {
			Type  string `json:"type"`
			Value struct {
				Id string `json:"id"`
			} `json:"value"`
		} `json:"screen"`
	} `json:"transition"`
}

// award
// Request from Rabbit
type PushFromRabbitReq struct {
	AwardType string `json:"award_type"`
	Title     string `json:"title"`
	Text      string `json:"text"`
	Label     string `json:"label"`
	ProfileId string `json:"profile_id"`
}

// response to Rabbit
type PushRabbitResp struct {
	Id      string `json:"id"`
	Event   string `json:"event"`
	Context struct {
		OfferId string `json:"offer_id"`
	} `json:"context"`
}

// Rabbit Request Data
type PushData struct {
	IsCustom   bool   `json:"isCustom"`
	Returnable bool   `json:"returnable"`
	Title      string `json:"title"`
	Text       string `json:"text"`
	Label      string `json:"label"`
}

// Rabbit Request Context
type PushContextFromRabbit struct {
	ProfileId string `json:"profile_id"`
	AwardType string `json:"award_type"`
}
