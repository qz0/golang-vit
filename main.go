package main

import (
	"accelera-custom-actions/logger"
	"accelera-custom-actions/rabbit"
	"accelera-custom-actions/redis"
	"accelera-custom-actions/utils"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
)

func main() {

	var err error

	utils.Cfg, err = utils.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config", err)
	}

	// выставляю уровень логов. Раскоментить желаемое что бы выбрать уровень с которого отображаются логи..
	if utils.Cfg.LogLevel == "DebugLevel" {
		logger.Log.SetLevel(log.DebugLevel)
		logger.Log.Warn("DebugLevel")
	} else {
		logger.Log.SetLevel(log.InfoLevel)
		logger.Log.Out = ioutil.Discard
	}

	//logger.Log.Warn("Current config: ", utils.Cfg)

	redis.ActionRegistration()

	rabbit.QueuesRegistration()

	rabbit.GetMessage(utils.Cfg.AwardQueueName)
	rabbit.GetMessage(utils.Cfg.OfferQueueName)
	rabbit.GetMessage(utils.Cfg.PushMessageQueueName)
	rabbit.GetMessage(utils.Cfg.PushDeviceQueueName)
	rabbit.GetMessage(utils.Cfg.PushOffersQueueName)
	rabbit.GetMessage(utils.Cfg.PushAwardQueueName)

	// Включаем роутер
	//http.CreateWebServer(9000, true)

}
